package com.vizzou.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "follow_friends")
public class FollowFriendBean {
	
	@Id
	@Column(name="fId")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int nId ;
	
	@Column(name="userid")
	private String strUserId;
	
	@Column(name="friendid")
	private String strFriendId;
	
	@Column(name="createdBy")
	private String strCreatedBy;
	
	@Column(name="createdon")
	private Date dtCreadtedOn;
	
	public int getnId() {
		return nId;
	}

	public void setnId(int nId) {
		this.nId = nId;
	}

	public String getStrUserId() {
		return strUserId;
	}

	public void setStrUserId(String strUserId) {
		this.strUserId = strUserId;
	}

	public String getStrFriendId() {
		return strFriendId;
	}

	public void setStrFriendId(String strFriendId) {
		this.strFriendId = strFriendId;
	}

	public String getStrCreatedBy() {
		return strCreatedBy;
	}

	public void setStrCreatedBy(String strCreatedBy) {
		this.strCreatedBy = strCreatedBy;
	}

	public Date getDtCreadtedOn() {
		return dtCreadtedOn;
	}

	public void setDtCreadtedOn(Date dtCreadtedOn) {
		this.dtCreadtedOn = dtCreadtedOn;
	}

	
}
