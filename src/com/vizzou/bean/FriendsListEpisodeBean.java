package com.vizzou.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "episodefriendssharing")
public class FriendsListEpisodeBean {
	
	@Id
	@Column(name="nfeid")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int nfeid;
	
	@Column(name="userid")
	private String strUserId;
	
	@Column(name="friendid")
	private String strFriendId;
	
	@Column(name="episodeid")
	private String strEpisodeId;
	
	@Column(name="created_on")
	private Date dtCreatedOn;
	
	@Column(name="created_by")
	private String strCreatedBy;

	@Column(name="modified_on")
	private Date dtModifiedOn;
	
	@Column(name="modified_by")
	private String strModifiedBy;
	
	public int getFeid() {
		return nfeid;
	}
	public void setFeid(int nfeid) {
		this.nfeid = nfeid;
	}
	public String getStrUserId() {
		return strUserId;
	}
	public void setStrUserId(String strUserId) {
		this.strUserId = strUserId;
	}
	public String getStrFriendId() {
		return strFriendId;
	}
	public void setStrFriendId(String strFriendId) {
		this.strFriendId = strFriendId;
	}
	public String getStrEpisodeId() {
		return strEpisodeId;
	}
	public void setStrEpisodeId(String strEpisodeId) {
		this.strEpisodeId = strEpisodeId;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public String getStrCreatedBy() {
		return strCreatedBy;
	}
	public void setStrCreatedBy(String strCreatedBy) {
		this.strCreatedBy = strCreatedBy;
	}
	public Date getDtModifiedOn() {
		return dtModifiedOn;
	}
	public void setDtModifiedOn(Date dtModifiedOn) {
		this.dtModifiedOn = dtModifiedOn;
	}
	public String getStrModifiedBy() {
		return strModifiedBy;
	}
	public void setStrModifiedBy(String strModifiedBy) {
		this.strModifiedBy = strModifiedBy;
	}
	
	
	
}
