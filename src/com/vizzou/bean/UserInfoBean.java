package com.vizzou.bean;

import java.util.Date;
import java.util.HashMap;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "userInfo")
public class UserInfoBean {

	@Id
	@Column(name="Id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	int nUid;
	
	@Column(name="UserID")
	String strUserId;
	
	@Column(name="firstname")
	String strFirstName;
	
	@Column(name="lastname")
	String strLastName;
	
	@Column(name="age")
	int nAge;
	
	@Column(name="gender")
	String strGender;
	
	@Column(name="email")
	String strEmail;
	
	@Column(name="latitude")
	double dLatitude;
	
	@Column(name="longitude")
	double dLongitude;
	
	@Column(name="userpassword")
	String strPassword;
	
	@Column(name="created_by")
	String strCreatedBy;

	@Column(name="modified_by")
	String strModifiedBy;
	
	@Column(name="created_on")
	Date dtCreatedOn;
	
	@Column(name="modified_on")
	Date dtModifiedOn;
	
	@Column(name="profilepic")
	String strProfilePic;
	
	@Column(name="phoneno")
	String strPhoneNo;
	
	public String getStrProfilePic() {
		return strProfilePic;
	}
	public void setStrProfilePic(String strProfilePic) {
		this.strProfilePic = strProfilePic;
	}

	public String getStrPhoneNo() {
		return strPhoneNo;
	}
	public void setStrPhoneNo(String strPhoneNo) {
		this.strPhoneNo = strPhoneNo;
	}


	public String getStrCreatedBy() {
		return strCreatedBy;
	}
	public void setStrCreatedBy(String strCreatedBy) {
		this.strCreatedBy = strCreatedBy;
	}
	public String getStrModifiedBy() {
		return strModifiedBy;
	}
	public void setStrModifiedBy(String strModifiedBy) {
		this.strModifiedBy = strModifiedBy;
	}
	public Date getDtCreatedOn() {
		return dtCreatedOn;
	}
	public void setDtCreatedOn(Date dtCreatedOn) {
		this.dtCreatedOn = dtCreatedOn;
	}
	public Date getDtModifiedOn() {
		return dtModifiedOn;
	}
	public void setDtModifiedOn(Date dtModifiedOn) {
		this.dtModifiedOn = dtModifiedOn;
	}
	
	public String getStrPassword() {
		return strPassword;
	}
	public void setStrPassword(String strPassword) {
		this.strPassword = strPassword;
	}
	public String getStrFirstName() {
		return strFirstName;
	}
	public void setStrFirstName(String strFirstName) {
		this.strFirstName = strFirstName;
	}
	public String getStrLastName() {
		return strLastName;
	}
	public void setStrLastName(String strLastName) {
		this.strLastName = strLastName;
	}
	public int getnAge() {
		return nAge;
	}
	public void setnAge(int nAge) {
		this.nAge = nAge;
	}
	public String getStrGender() {
		return strGender;
	}
	public void setStrGender(String strGender) {
		this.strGender = strGender;
	}
	public String getStrEmail() {
		return strEmail;
	}
	public void setStrEmail(String strEmail) {
		this.strEmail = strEmail;
	}
	public double getdLatitude() {
		return dLatitude;
	}
	public void setdLatitude(double dLatitude) {
		this.dLatitude = dLatitude;
	}
	public double getdLongitude() {
		return dLongitude;
	}
	public void setdLongitude(double dLongitude) {
		this.dLongitude = dLongitude;
	}
	public int getnUid() {
		return nUid;
	}
	public void setnUid(int nUid) {
		this.nUid = nUid;
	}
	public String getStrUserId() {
		return strUserId;
	}
	public void setStrUserId(String strUserId) {
		this.strUserId = strUserId;
	}
	
	@Override
    public String toString() {
		HashMap<String, String> hUserData = new HashMap<String, String>();
		hUserData.put("FirstName", strFirstName);
		hUserData.put("LastName", strLastName);
		hUserData.put("Email", strEmail);
		hUserData.put("UserId", strUserId);
		//String strReturn =  "{ \"UId\" : " + nUid +",\"UserId\" : " + strUserId +",\"FirstName\" : " + strFirstName +",\"LastName\" : " + strLastName +",\"Age\" : " + nAge +",\"Email\" : " + strEmail+"}";
        return hUserData.toString();

    }

}
