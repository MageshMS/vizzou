package com.vizzou.bean;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "episodeinfo")
public class EpisodeInfoBean {
	
	@Id
	@Column(name="eId")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int nId ;
	
	@Id
	@Column(name="episodeId")
	@GeneratedValue(generator = "hibernate-uuid")
	@GenericGenerator(name = "hibernate-uuid", strategy = "uuid2")
	private String strVideoId ;
	
	@Column(name="userid")
	private String strUserId;
	
	@Column(name="videoname")
	private String strvideoname;
	
	@Column(name="videorefname")
	private String strVideoRefName;
	
	@Column(name="imagename")
	private String strImageName;
	
	@Column(name="created_by")
	private String strCreated_by;
	
	@Column(name="modified_by")
	private String strModified_by;
	
	@Column(name="created_on")
	private Date dCreated_On;
	
	@Column(name="modified_on")
	private Date dModified_On;
	
	@Column(name="totalviewcount")
	private int nTotalViewCount;
	
	@Column(name="tag")
	private String strTagName;
	
	@Column(name="keywords")
	private String strKeyWords;

	@Column(name="releasedate")
	private Date dtReleaseDate;
	
	@Column(name="episodename")
	private String strEpisodeName;
	
	public String getStrKeyWords() {
		return strKeyWords;
	}

	public void setStrKeyWords(String strKeyWords) {
		this.strKeyWords = strKeyWords;
	}

	public Date getDtReleaseDate() {
		return dtReleaseDate;
	}

	public void setDtReleaseDate(Date dtReleaseDate) {
		this.dtReleaseDate = dtReleaseDate;
	}

	public String getStrEpisodeName() {
		return strEpisodeName;
	}

	public void setStrEpisodeName(String strEpisodeName) {
		this.strEpisodeName = strEpisodeName;
	}
	public int getnTotalViewCount() {
		return nTotalViewCount;
	}

	public void setnTotalViewCount(int nTotalViewCount) {
		this.nTotalViewCount = nTotalViewCount;
	}

	public String getStrTagName() {
		return strTagName;
	}

	public void setStrTagName(String strTagName) {
		this.strTagName = strTagName;
	}


	public String getStrImageName() {
		return strImageName;
	}

	public void setStrImageName(String strImageName) {
		this.strImageName = strImageName;
	}
	
	public int getnId() {
		return nId;
	}

	public void setnId(int nId) {
		this.nId = nId;
	}
	public String getStrVideoId() {
		return strVideoId;
	}

	public void setStrVideoId(String strVideoId) {
		this.strVideoId = strVideoId;
	}

	public String getStrUserId() {
		return strUserId;
	}

	public void setStrUserId(String strUserId) {
		this.strUserId = strUserId;
	}

	public String getStrvideoname() {
		return strvideoname;
	}

	public void setStrvideoname(String strvideoname) {
		this.strvideoname = strvideoname;
	}

	public String getStrVideoRefName() {
		return strVideoRefName;
	}

	public void setStrVideoRefName(String strVideoRefName) {
		this.strVideoRefName = strVideoRefName;
	}

	public String getStrCreated_by() {
		return strCreated_by;
	}

	public void setStrCreated_by(String strCreated_by) {
		this.strCreated_by = strCreated_by;
	}

	public String getStrModified_by() {
		return strModified_by;
	}

	public void setStrModified_by(String strModified_by) {
		this.strModified_by = strModified_by;
	}

	public Date getdCreated_On() {
		return dCreated_On;
	}

	public void setdCreated_On(Date dCreated_On) {
		this.dCreated_On = dCreated_On;
	}

	public Date getdModified_On() {
		return dModified_On;
	}

	public void setdModified_On(Date dModified_On) {
		this.dModified_On = dModified_On;
	}

	
}
