package com.vizzou.model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.Date;

import com.vizzou.utils.UtilsFactory;

public class UploadFileModel {

	public static String uploadVideo(InputStream uploadedInputStream,String fileName, String strUploadFileDirectory, boolean bIsVideoFile) {
		String fileLocation = "";
		try{
			String strDate = UtilsFactory.formatDateTimeToddMMyyyyHHMMSS( new Date().toString() );
			//System.out.println("Inside");
			String[] tokens = fileName.split("\\.(?=[^\\.]+$)");
			if( bIsVideoFile ){
				fileName = tokens[0]+"_"+strDate+"."+tokens[1];
				fileName = strDate+"."+tokens[1];
			}
			
			fileLocation = strUploadFileDirectory + fileName;
			FileOutputStream out = new FileOutputStream(new File(fileLocation));  
            int read = 0;  
            byte[] bytes = new byte[1024];  
            out = new FileOutputStream(new File(fileLocation));  
            while ((read = uploadedInputStream.read(bytes)) != -1) {  
                out.write(bytes, 0, read);  
            } 
            
            out.flush();  
            out.close();
		}catch( Exception e){
			System.out.println("Exception is in uploadVideo : " + e.getMessage());
			e.printStackTrace();
		}
		return fileName;
	}

}
