package com.vizzou.model;

import java.sql.ResultSet;
import java.sql.Statement;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.vizzou.dao.CommonDAO;
import com.vizzou.utils.DataBaseManager;

public class EpisodeDetailsModel {

	public static JSONArray getEpisodeDetails(Statement statement, int nEpisodeId, String strUserId) throws Exception {
		ResultSet rs = null;
		StringBuffer sbQuery = null;
		JSONArray jaEpisodeDetails = null;
		JSONObject joEpisodeDetail = null;
		
		try{
			sbQuery= new StringBuffer();
			jaEpisodeDetails = new JSONArray();
			
			sbQuery.append("select vi.*, um.firstname, um.lastname from episodeinfo AS vi INNER JOIN userinfo AS um ON um.userID = vi.userid where vi.userid = '")
					.append(strUserId).append("' AND eId = ").append(nEpisodeId);
			System.out.println(sbQuery.toString());
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			while( rs.next() ){
				joEpisodeDetail = new JSONObject();
				joEpisodeDetail.put("EpisodeId", rs.getString("eId"));
				joEpisodeDetail.put("UserId", rs.getString("userid"));
				joEpisodeDetail.put("UserFirstName", rs.getString("firstname"));
				joEpisodeDetail.put("UserLastName", rs.getString("lastname"));
				joEpisodeDetail.put("EpisodeName", rs.getString("episodename"));
				joEpisodeDetail.put("ReleaseDate", rs.getString("releasedate"));
				joEpisodeDetail.put("Keywords", rs.getString("keywords"));
				joEpisodeDetail.put("VideoName", rs.getString("videoname"));
				joEpisodeDetail.put("VideoRefName", rs.getString("videorefname"));
				joEpisodeDetail.put("CreatedOn", rs.getString("created_on"));
				joEpisodeDetail.put("ThumbImg", rs.getString("imagename"));
				joEpisodeDetail.put("Tag", rs.getString("tag"));
				joEpisodeDetail.put("TotalViewCount", rs.getString("totalviewcount"));
				joEpisodeDetail.put("Created_on", rs.getString("created_on"));
				joEpisodeDetail.put("Created_By", rs.getString("created_by"));
				joEpisodeDetail.put("Modified_On", rs.getString("modified_by"));
				joEpisodeDetail.put("Modified_On", rs.getString("modified_on"));
				jaEpisodeDetails.add(joEpisodeDetail);
				
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}
		return jaEpisodeDetails;
	}

	public static JSONArray getEpisodeDetailsBasedOnTag(Statement statement,String strTag, String strUserId, int nLimit, int nOffset) throws Exception {
		ResultSet rs = null;
		StringBuffer sbQuery = null;
		JSONArray jaEpisodeDetails = null;
		JSONObject joEpisodeDetail = null;
		
		try{
			sbQuery= new StringBuffer();
			jaEpisodeDetails = new JSONArray();
			
			sbQuery.append("select vi.*, um.firstname, um.lastname from episodeinfo AS vi INNER JOIN userinfo AS um ON um.userID = vi.userid where vi.userid = '")
					.append(strUserId).append("' AND tag = '").append(strTag).append("' ORDER BY releasedate DESC ");
			if(nLimit > 0 ){
				sbQuery.append(" LIMIT ").append(nLimit).append(" OFFSET ").append(nOffset);
			}
			System.out.println(sbQuery.toString());
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			while( rs.next() ){
				joEpisodeDetail = new JSONObject();
				joEpisodeDetail.put("EpisodeId", rs.getString("eId"));
				joEpisodeDetail.put("UserId", rs.getString("userid"));
				joEpisodeDetail.put("UserFirstName", rs.getString("firstname"));
				joEpisodeDetail.put("UserLastName", rs.getString("lastname"));
				joEpisodeDetail.put("EpisodeName", rs.getString("episodename"));
				joEpisodeDetail.put("ReleaseDate", rs.getString("releasedate"));
				joEpisodeDetail.put("Keywords", rs.getString("keywords"));
				joEpisodeDetail.put("VideoName", rs.getString("videoname"));
				joEpisodeDetail.put("VideoRefName", rs.getString("videorefname"));
				joEpisodeDetail.put("CreatedOn", rs.getString("created_on"));
				joEpisodeDetail.put("ThumbImg", rs.getString("imagename"));
				joEpisodeDetail.put("Tag", rs.getString("tag"));
				joEpisodeDetail.put("TotalViewCount", rs.getString("totalviewcount"));
				joEpisodeDetail.put("Created_on", rs.getString("created_on"));
				joEpisodeDetail.put("Created_By", rs.getString("created_by"));
				joEpisodeDetail.put("Modified_On", rs.getString("modified_by"));
				joEpisodeDetail.put("Modified_On", rs.getString("modified_on"));
				jaEpisodeDetails.add(joEpisodeDetail);
				
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}
		return jaEpisodeDetails;
	}

	public static int getTotalEpisodeDetailsBasedOnTag(Statement statement,
			String strTag, String strUserId) throws Exception {
		ResultSet rs = null;
		StringBuffer sbQuery = null;
		int nTotalCount = 0;
		
		try{
			sbQuery= new StringBuffer();			
			sbQuery.append("select count(*) AS nTotalCount from episodeinfo AS vi INNER JOIN userinfo AS um ON um.userID = vi.userid where vi.userid = '")
					.append(strUserId).append("' AND tag = '").append(strTag).append("' ORDER BY releasedate DESC ");
			
			System.out.println(sbQuery.toString());
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			if( rs.next() ){
				nTotalCount = rs.getInt("nTotalCount");
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}
		return nTotalCount;
	}

	public static int getTotalFriendsEpisodeDetails(Statement statement,
			String strTag, String strUserId) throws Exception {
		ResultSet rs = null;
		StringBuffer sbQuery = null;
		int nTotalCount = 0;
		
		try{
			sbQuery= new StringBuffer();			
			sbQuery.append("select count(*) AS nTotalCount from episodefriendssharing AS epif INNER JOIN episodeinfo AS epi ON epi.eId = epif.episodeid where epif.friendid = '")
					.append(strUserId).append("' ORDER BY epi.releasedate DESC ");
			
			System.out.println(sbQuery.toString());
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			if( rs.next() ){
				nTotalCount = rs.getInt("nTotalCount");
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}
		return nTotalCount;
	}

	public static JSONArray getFriendsEpisodeDetails(Statement statement, String strTag, String strUserId, int nLimit, int nOffset) throws Exception {
		ResultSet rs = null;
		StringBuffer sbQuery = null;
		JSONArray jaEpisodeDetails = null;
		JSONObject joEpisodeDetail = null;
		
		try{
			sbQuery= new StringBuffer();
			jaEpisodeDetails = new JSONArray();
			
			sbQuery.append("select epi.* from episodefriendssharing AS epif INNER JOIN episodeinfo AS epi ON epi.eId = epif.episodeid where epif.friendid = '")
			.append(strUserId).append("' ORDER BY epi.releasedate DESC ");
			if(nLimit > 0 ){
				sbQuery.append(" LIMIT ").append(nLimit).append(" OFFSET ").append(nOffset);
			}
			System.out.println(sbQuery.toString());
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			while( rs.next() ){
				joEpisodeDetail = new JSONObject();
				joEpisodeDetail.put("EpisodeId", rs.getString("eId"));
				joEpisodeDetail.put("UserId", rs.getString("userid"));
				joEpisodeDetail.put("EpisodeName", rs.getString("episodename"));
				joEpisodeDetail.put("ReleaseDate", rs.getString("releasedate"));
				joEpisodeDetail.put("Keywords", rs.getString("keywords"));
				joEpisodeDetail.put("VideoName", rs.getString("videoname"));
				joEpisodeDetail.put("VideoRefName", rs.getString("videorefname"));
				joEpisodeDetail.put("CreatedOn", rs.getString("created_on"));
				joEpisodeDetail.put("ThumbImg", rs.getString("imagename"));
				joEpisodeDetail.put("Tag", rs.getString("tag"));
				joEpisodeDetail.put("TotalViewCount", rs.getString("totalviewcount"));
				joEpisodeDetail.put("Created_on", rs.getString("created_on"));
				joEpisodeDetail.put("Created_By", rs.getString("created_by"));
				joEpisodeDetail.put("Modified_On", rs.getString("modified_by"));
				joEpisodeDetail.put("Modified_On", rs.getString("modified_on"));
				jaEpisodeDetails.add(joEpisodeDetail);
				
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}
		return jaEpisodeDetails;
	}

	public static int getTotalOpenEpisodeDetails(Statement statement,	String strTag, String strUserId) throws Exception {
		ResultSet rs = null;
		StringBuffer sbQuery = null;
		int nTotalCount = 0;
		
		try{
			sbQuery= new StringBuffer();			
			sbQuery.append("select count(*) AS nTotalCount from episodeinfo AS vi INNER JOIN userinfo AS um ON um.userID = vi.userid where vi.tag = '").append(strTag).append("' ORDER BY releasedate DESC ");
			
			System.out.println(sbQuery.toString());
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			if( rs.next() ){
				nTotalCount = rs.getInt("nTotalCount");
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}
		return nTotalCount;
	}

	public static JSONArray getOpenEpisodeDetails(Statement statement,	String strTag, String strUserId, int nLimit, int nOffset) throws Exception {
		ResultSet rs = null;
		StringBuffer sbQuery = null;
		JSONArray jaEpisodeDetails = null;
		JSONObject joEpisodeDetail = null;
		
		try{
			sbQuery= new StringBuffer();
			jaEpisodeDetails = new JSONArray();
			
			sbQuery.append("select vi.*, um.firstname, um.lastname from episodeinfo AS vi INNER JOIN userinfo AS um ON um.userID = vi.userid where vi.tag = '").append(strTag).append("' ORDER BY releasedate DESC ");
			if(nLimit > 0 ){
				sbQuery.append(" LIMIT ").append(nLimit).append(" OFFSET ").append(nOffset);
			}
			System.out.println(sbQuery.toString());
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			while( rs.next() ){
				joEpisodeDetail = new JSONObject();
				joEpisodeDetail.put("EpisodeId", rs.getString("eId"));
				joEpisodeDetail.put("UserId", rs.getString("userid"));
				joEpisodeDetail.put("UserFirstName", rs.getString("firstname"));
				joEpisodeDetail.put("UserLastName", rs.getString("lastname"));
				joEpisodeDetail.put("EpisodeName", rs.getString("episodename"));
				joEpisodeDetail.put("ReleaseDate", rs.getString("releasedate"));
				joEpisodeDetail.put("Keywords", rs.getString("keywords"));
				joEpisodeDetail.put("VideoName", rs.getString("videoname"));
				joEpisodeDetail.put("VideoRefName", rs.getString("videorefname"));
				joEpisodeDetail.put("CreatedOn", rs.getString("created_on"));
				joEpisodeDetail.put("ThumbImg", rs.getString("imagename"));
				joEpisodeDetail.put("Tag", rs.getString("tag"));
				joEpisodeDetail.put("TotalViewCount", rs.getString("totalviewcount"));
				joEpisodeDetail.put("Created_on", rs.getString("created_on"));
				joEpisodeDetail.put("Created_By", rs.getString("created_by"));
				joEpisodeDetail.put("Modified_On", rs.getString("modified_by"));
				joEpisodeDetail.put("Modified_On", rs.getString("modified_on"));
				jaEpisodeDetails.add(joEpisodeDetail);
				
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}
		return jaEpisodeDetails;
	}

	public static int getTotalOpenEpisodes(Statement statement, String strQuery) {
		ResultSet rs = null;
		StringBuffer sbQuery = null;
		int nTotalCount = 0;
		
		try{
			sbQuery= new StringBuffer();			
			sbQuery.append("select count(*) AS nTotalCount from episodeinfo AS vi INNER JOIN userinfo AS um ON um.userID = vi.userid where vi.keyword like '%").append(strQuery).append("%' ORDER BY releasedate DESC ");
			
			System.out.println(sbQuery.toString());
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			if( rs.next() ){
				nTotalCount = rs.getInt("nTotalCount");
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}	
		return nTotalCount;
	}

	public static JSONArray getOpenEpisodes(Statement statement,
			String strQuery, int nLimit, int nOffset) {
			ResultSet rs = null;
			StringBuffer sbQuery = null;
			JSONArray jaEpisodeDetails = null;
			JSONObject joEpisodeDetail = null;
			
			try{
				sbQuery= new StringBuffer();
				jaEpisodeDetails = new JSONArray();
				
				sbQuery.append("select vi.*, um.firstname, um.lastname from episodeinfo AS vi INNER JOIN userinfo AS um ON um.userID = vi.userid where vi.keyword like '%").append(strQuery).append("%' ORDER BY releasedate DESC ");
				if(nLimit > 0 ){
					sbQuery.append(" LIMIT ").append(nLimit).append(" OFFSET ").append(nOffset);
				}
				System.out.println(sbQuery.toString());
				rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
				while( rs.next() ){
					joEpisodeDetail = new JSONObject();
					joEpisodeDetail.put("EpisodeId", rs.getString("eId"));
					joEpisodeDetail.put("UserId", rs.getString("userid"));
					joEpisodeDetail.put("UserFirstName", rs.getString("firstname"));
					joEpisodeDetail.put("UserLastName", rs.getString("lastname"));
					joEpisodeDetail.put("EpisodeName", rs.getString("episodename"));
					joEpisodeDetail.put("ReleaseDate", rs.getString("releasedate"));
					joEpisodeDetail.put("Keywords", rs.getString("keywords"));
					joEpisodeDetail.put("VideoName", rs.getString("videoname"));
					joEpisodeDetail.put("VideoRefName", rs.getString("videorefname"));
					joEpisodeDetail.put("CreatedOn", rs.getString("created_on"));
					joEpisodeDetail.put("ThumbImg", rs.getString("imagename"));
					joEpisodeDetail.put("Tag", rs.getString("tag"));
					joEpisodeDetail.put("TotalViewCount", rs.getString("totalviewcount"));
					joEpisodeDetail.put("Created_on", rs.getString("created_on"));
					joEpisodeDetail.put("Created_By", rs.getString("created_by"));
					joEpisodeDetail.put("Modified_On", rs.getString("modified_by"));
					joEpisodeDetail.put("Modified_On", rs.getString("modified_on"));
					jaEpisodeDetails.add(joEpisodeDetail);
					
				}
			}catch(Exception ex){
				ex.printStackTrace();
			}finally{
				DataBaseManager.closeResultSet(rs);	
			}
			return jaEpisodeDetails;
	}

}
