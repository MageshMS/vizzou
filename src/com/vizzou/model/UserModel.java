package com.vizzou.model;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.vizzou.bean.FollowFriendBean;
import com.vizzou.bean.FriendsListEpisodeBean;
import com.vizzou.bean.UserInfoBean;
import com.vizzou.bean.EpisodeInfoBean;
import com.vizzou.dao.CommonDAO;
import com.vizzou.dao.UtilsDAO;
import com.vizzou.utils.Constants;
import com.vizzou.utils.DataBaseManager;
import com.vizzou.utils.UtilsFactory;

public class UserModel {

	public static UserInfoBean createNewUser(UserInfoBean userBean) {
		
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml"); 
			UtilsDAO utilModel = (UtilsDAO)context1.getBean("utildao");
			utilModel.saveObject(userBean);
			if( userBean.getnUid() > 0){
				userBean.setStrCreatedBy("vizzou"+userBean.getnUid());
				userBean.setStrUserId( "vizzou"+userBean.getnUid() );
				
				utilModel.updateJob(userBean);
			}
		}catch ( Exception ex){
			ex.printStackTrace();
			throw ex;
		}
		return userBean;
	}
	public static UserInfoBean updateUserProfile(UserInfoBean userBean) {
		
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml"); 
			UtilsDAO utilModel = (UtilsDAO)context1.getBean("utildao");
			utilModel.updateJob(userBean);
		}catch ( Exception ex){
			ex.printStackTrace();
			throw ex;
		}
		return userBean;
	}

	public static boolean isUserAlreadyExist(Statement statement, UserInfoBean userBean) throws Exception {
		ResultSet rs = null;
		StringBuffer sbQuery = null;
		boolean bIsExist = false;
		try{
			sbQuery= new StringBuffer();
			sbQuery.append("select * from userInfo where email = '").append(userBean.getStrEmail()).append("'");
			//System.out.println( sbQuery.toString() );
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			if( rs.next() ){
				bIsExist = true;
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}
		return bIsExist;
	}

	public static JSONArray loginValidation(Statement statement, UserInfoBean userBean) throws Exception {
		ResultSet rs = null;
		StringBuffer sbQuery = null;
		JSONArray jaLoginDetails = null;
		JSONObject joLoginDetail = null;
		
		try{
			sbQuery= new StringBuffer();
			jaLoginDetails = new JSONArray();
			
			sbQuery.append("select * from userInfo where email = '").append(userBean.getStrEmail())
				.append("' AND userpassword = '").append(userBean.getStrPassword()).append("'");
			
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			while( rs.next() ){
				joLoginDetail = new JSONObject();
				joLoginDetail.put("FirstName", rs.getString("firstname"));
				joLoginDetail.put("LastName", rs.getString("lastname"));
				joLoginDetail.put("Email", rs.getString("email"));
				joLoginDetail.put("Latitude", rs.getString("longitude"));
				joLoginDetail.put("Longitude", rs.getString("latitude"));
				joLoginDetail.put("ProfilePic", rs.getString("profilepic"));
				joLoginDetail.put("UserId", rs.getString("UserID"));
				jaLoginDetails.add(joLoginDetail);
				
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}
		return jaLoginDetails;
	}

	public static JSONObject saveVideoInfo(EpisodeInfoBean vBean) {
		JSONObject joReturn = null;
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml"); 
			UtilsDAO utilModel = (UtilsDAO)context1.getBean("utildao");
			utilModel.saveObject(vBean);
			String strFilePath = Constants.strDownloadFileDirectory + vBean.getStrvideoname();
			String strImageFilePath = Constants.strImageDownloadFileDirectory + vBean.getStrImageName();
			HashMap<String, String> hVideoInfo = new HashMap<String, String>();
			hVideoInfo.put("VideoFileName", vBean.getStrvideoname());
			//hVideoInfo.put("VideoPath", strFilePath);
			hVideoInfo.put("ReleaseDate", vBean.getDtReleaseDate()+"");
			hVideoInfo.put("EpisodeName", vBean.getStrEpisodeName());
			hVideoInfo.put("Tag", vBean.getStrTagName());
			hVideoInfo.put("VideoFileRefName", vBean.getStrVideoRefName());
			hVideoInfo.put("ImageFile", strImageFilePath);
			hVideoInfo.put("CreatedOn", vBean.getdCreated_On()+"");
			joReturn = UtilsFactory.getJSONSuccessReturn("VideoInfo", hVideoInfo);
		}catch ( Exception ex){
			joReturn = UtilsFactory.getJSONFailureReturn(ex.getMessage());
			ex.printStackTrace();
			throw ex;
		}
		return joReturn;
	}
	
	/*
	 * Function to total of episodes.
	 * Return : int
	 * Parameter: DB connection - Statement
	 */
	/*public static int getTotalAllEpisodes(Statement statement) throws Exception {
		ResultSet rs = null;
		int nTotalCount = 0;
		StringBuffer sbQuery = null;
		
		try{
			sbQuery= new StringBuffer();			
			sbQuery.append("select count(*) as nTotalCount from videofileinfo ");
			
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			while( rs.next() ){
				nTotalCount = rs.getInt("nTotalCount");
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}
		return nTotalCount;
	}*/

	/*public static JSONArray getAllEpisodes(Statement statement) throws Exception {
		ResultSet rs = null;
		StringBuffer sbQuery = null;
		JSONArray jaEpisodeDetails = null;
		JSONObject joEpisodeDetail = null;
		
		try{
			sbQuery= new StringBuffer();
			jaEpisodeDetails = new JSONArray();
			
			sbQuery.append("select vi.*, um.firstname, um.lastname from videofileinfo AS vi INNER JOIN userinfo AS um ON um.userID = vi.userid ");
			System.out.println(sbQuery.toString());
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			while( rs.next() ){
				joEpisodeDetail = new JSONObject();
				joEpisodeDetail.put("VedioId", rs.getString("Id"));
				joEpisodeDetail.put("UserId", rs.getString("userid"));
				joEpisodeDetail.put("UserFirstName", rs.getString("firstname"));
				joEpisodeDetail.put("UserLastName", rs.getString("lastname"));
				joEpisodeDetail.put("VideoName", rs.getString("videoname"));
				joEpisodeDetail.put("VideoRefName", rs.getString("videorefname"));
				joEpisodeDetail.put("CreatedOn", rs.getString("created_on"));
				joEpisodeDetail.put("ThumbImg", rs.getString("imagename"));
				joEpisodeDetail.put("Tag", rs.getString("tag"));
				joEpisodeDetail.put("TotalViewCount", rs.getString("totalviewcount"));
				jaEpisodeDetails.add(joEpisodeDetail);
				
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}
		return jaEpisodeDetails;
	}*/

	public static JSONObject saveFriendsShareInfo(List<FriendsListEpisodeBean> alFriends) {
		JSONObject joReturn = null;
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml"); 
			UtilsDAO utilModel = (UtilsDAO)context1.getBean("utildao");
			utilModel.saveCollectionAllObject(alFriends);
			joReturn = UtilsFactory.getJSONSuccessReturn("Successfully added");
		}catch( Exception e){
			joReturn = UtilsFactory.getJSONFailureReturn(e.getMessage());
			System.out.println("Exception is in saveFriendsShareInfo : "+ e.getMessage());
			e.printStackTrace();
		}
		return joReturn;
	}

	public static JSONObject updateEpisode(EpisodeInfoBean vBean, Statement statement) {

		int nUpdated = 0;
		StringBuffer sbQuery = null;
		JSONObject joReturn = null;
		try{
			sbQuery= new StringBuffer();
			
			sbQuery.append("update episodeinfo SET videoname = '").append(vBean.getStrvideoname()).append("', videorefname = '")
				.append(vBean.getStrVideoRefName()).append("' WHERE eId = ").append(vBean.getnId());
			System.out.println(sbQuery.toString());
			nUpdated = CommonDAO.toUpdateobjects( statement, sbQuery.toString());
			if( nUpdated> 0 ){
				joReturn = UtilsFactory.getJSONSuccessReturn("Successfully updated !!!");
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}
		return joReturn;
	}

	public static boolean isEpisodeNameIsExist(Statement statement, String strEpisodeName) throws Exception {
		ResultSet rs = null;
		StringBuffer sbQuery = null;
		boolean bResult = false;
		
		try{
			sbQuery= new StringBuffer();
			
			sbQuery.append("select * from episodeinfo Where episodename = '").append(strEpisodeName).append("'");
		//	System.out.println(sbQuery.toString());
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			if( rs.next() ){
				bResult = true;
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}
		return bResult;
	}

	public static int getTotalUserCount(Statement statement) throws Exception {
		ResultSet rs = null;
		int nTotalCount = 0;
		StringBuffer sbQuery = null;
		
		try{
			sbQuery= new StringBuffer();			
			sbQuery.append("select count(*) as nTotalCount from userinfo ");
			
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			while( rs.next() ){
				nTotalCount = rs.getInt("nTotalCount");
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}
		return nTotalCount;
	}

	public static JSONArray getUserDetails(Statement statement, int nLimit,	int nOffset) throws Exception {
		ResultSet rs = null;
		StringBuffer sbQuery = null;
		JSONArray jaLoginDetails = null;
		JSONObject joLoginDetail = null;
		
		try{
			sbQuery= new StringBuffer();
			jaLoginDetails = new JSONArray();
			
			sbQuery.append("select * from userInfo ORDER BY firstname ASC ");
			
			if( nLimit>0 ){
				sbQuery.append(" LIMIT ").append(nLimit).append(" OFFSET ").append(nOffset);
			}
			
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			while( rs.next() ){
				joLoginDetail = new JSONObject();
				joLoginDetail.put("FirstName", rs.getString("firstname"));
				joLoginDetail.put("LastName", rs.getString("lastname"));
				joLoginDetail.put("Email", rs.getString("email"));
				joLoginDetail.put("Latitude", rs.getString("longitude"));
				joLoginDetail.put("Longitude", rs.getString("latitude"));
				joLoginDetail.put("UserId", rs.getString("UserID"));
				jaLoginDetails.add(joLoginDetail);
				
			}
		}catch(Exception ex){
			ex.printStackTrace();
			throw ex;
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}
		return jaLoginDetails;
	}
	public static JSONArray getUserProfile(Statement statement, String strUserId) {
		ResultSet rs = null;
		StringBuffer sbQuery = null;
		JSONArray jaLoginDetails = null;
		JSONObject joLoginDetail = null;
		
		try{
			sbQuery= new StringBuffer();
			jaLoginDetails = new JSONArray();
			
			sbQuery.append("select * from userInfo where UserID = '").append(strUserId).append("'");
			
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			while( rs.next() ){
				joLoginDetail = new JSONObject();
				joLoginDetail.put("FirstName", rs.getString("firstname"));
				joLoginDetail.put("LastName", rs.getString("lastname"));
				joLoginDetail.put("Email", rs.getString("email"));
				joLoginDetail.put("Latitude", rs.getString("longitude"));
				joLoginDetail.put("Longitude", rs.getString("latitude"));
				joLoginDetail.put("UserId", rs.getString("UserID"));
				jaLoginDetails.add(joLoginDetail);
				
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}
		return jaLoginDetails;
	}
	public static JSONArray getSearchFriends(Statement statement,String strQuery, String strUserId) {
		ResultSet rs = null;
		StringBuffer sbQuery = null;
		JSONArray jaLoginDetails = null;
		JSONObject joLoginDetail = null;
		
		try{
			sbQuery= new StringBuffer();
			jaLoginDetails = new JSONArray();
			
			sbQuery.append("select um.*, IFNULL(fId, 0) AS followId from userInfo as um LEFT JOIN follow_friends as fl ON fl.friendid = um.UserID where CONCAT(um.firstname,' ',um.lastname)  LIKE  '%")
					.append(strQuery).append("%'")
					.append(" AND um.UserID != '").append(strUserId).append("' ")
					.append("ORDER BY CONCAT(firstname,' ',lastname) ASC ");
			System.out.println(sbQuery.toString());
			rs = CommonDAO.toGetobjects( statement, sbQuery.toString());
			while( rs.next() ){
				joLoginDetail = new JSONObject();
				joLoginDetail.put("FirstName", rs.getString("firstname"));
				joLoginDetail.put("LastName", rs.getString("lastname"));
				joLoginDetail.put("Email", rs.getString("email"));
				joLoginDetail.put("Latitude", rs.getString("longitude"));
				joLoginDetail.put("Longitude", rs.getString("latitude"));
				joLoginDetail.put("UserId", rs.getString("UserID"));
				joLoginDetail.put("FollowId", rs.getString("followId"));
				jaLoginDetails.add(joLoginDetail);
				
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			DataBaseManager.closeResultSet(rs);	
		}
		return jaLoginDetails;
	}
	public static boolean setFollowingFriend( FollowFriendBean followBean ) {
		boolean bReturn = false;
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml"); 
			UtilsDAO utilModel = (UtilsDAO)context1.getBean("utildao");
			utilModel.saveObject(followBean);
			bReturn = true;
			
		}catch ( Exception ex){
			ex.printStackTrace();
			throw ex;
		}
		return bReturn;
	}
	
	public static boolean removeFollowingFriend(FollowFriendBean followBean) {
		boolean bReturn = false;
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml"); 
			UtilsDAO utilModel = (UtilsDAO)context1.getBean("utildao");
			utilModel.deleteJob(followBean);
			bReturn = true;
		}catch ( Exception ex){
			ex.printStackTrace();
			throw ex;
		}
		return bReturn;
	}
	

}
