package com.vizzou.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DataBaseManager {
	private String connectionUrl;	
	private String dbUserName;
	private String dbPassword;
	
	public String getConnectionUrl() 
	{
		return connectionUrl;
	}
	
	public void setConnectionUrl(String connectionUrl) 
	{
		this.connectionUrl = connectionUrl;
	}
	
	public String getDbUserName() {
		return dbUserName;
	}

	public void setDbUserName(String dbUserName) {
		this.dbUserName = dbUserName;
	}

	public String getDbPassword() {
		return dbPassword;
	}

	public void setDbPassword(String dbPassword) {
		this.dbPassword = dbPassword;
	}

	
	public Connection giveConnection () throws SQLException{
		Connection connection = null;
		try{
			if( connection==null ){
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				connection = DriverManager.getConnection(connectionUrl, dbUserName, dbPassword);
			}
		}catch( Exception e){
			connection.close();
			e.printStackTrace();		
		}
		return connection;
	}
	public static boolean closeResultSet(ResultSet rst){
		try{
			if(rst != null )
				rst.close();
		}catch(Exception e){
			System.out.println("Exception while closing ResultSet: "+e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public static boolean closeStatement(Statement sta){
		try{
			if(sta != null )
				sta.close();
		}catch(Exception e){
			System.out.println("Exception while closing Statement: "+e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}
	public static boolean closeConnection(Connection conn){
		try{
			if(conn != null )
				conn.close();
		}catch(Exception e){
			System.out.println("Exception while closing Connection: "+e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
