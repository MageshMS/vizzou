package com.vizzou.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.json.simple.JSONObject;


public class UtilsFactory {
	public static JSONObject getJSONSuccessReturn( String strMessage ){
		JSONObject joReturn = new JSONObject();
		JSONObject joData = new JSONObject(); 
		
		try {
			joReturn.put("success", true);
			joReturn.put("failure", false);
			joData.put("message", strMessage);
			joReturn.put("data", joData);
		} catch (Exception e) {
			e.printStackTrace();
		}	
		
		return joReturn;
	}
	
	public static JSONObject getJSONFailureReturn( String strMessage ){
		JSONObject joReturn = new JSONObject();
		try {
			joReturn.put("success", false);
			joReturn.put("failure", true);
			joReturn.put("errorMessage", (strMessage+"").replaceAll("\"","\\\""));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
		return joReturn;
	}
	public static JSONObject getJSONSuccessReturn( String strTitle , Object obj){
		JSONObject joReturn = new JSONObject();
		JSONObject joData = null; 
		try {
			joReturn.put("success", true);
			joReturn.put("failure", false);
			joData = new JSONObject();
			//System.out.println(" : " + obj);
			joReturn.put("data", obj);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return joReturn;
	}
	
	public static String formatDateTimeToddMMyyyyHHMMSS(String ipDate){
		String opDate = "";
		try{
			DateFormat ipFormatter = new SimpleDateFormat("EEE MMM dd kk:mm:ss z yyyy");
			DateFormat opFormatter = new SimpleDateFormat("ddMMyyHHmmSS");
			opDate = opFormatter.format(ipFormatter.parse(ipDate));
		}catch(Exception e){
			System.out.println("Exception in formatDateTimeToddMMyyyyHHMMSS(): "+e.getMessage());
			e.printStackTrace();
		}
		return opDate;
	}
	
	public static Date formatDateTimeTommddyyyyhmma(String ipDate){
		SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy hh:mm a");
		Date date = null;
		try {
			formatter.setTimeZone(TimeZone.getTimeZone("IST"));
			System.out.println(ipDate);
			date = formatter.parse(ipDate);
			//System.out.println(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return date;
	}
}
