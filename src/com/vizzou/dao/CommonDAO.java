package com.vizzou.dao;

import java.sql.ResultSet;
import java.sql.Statement;

public class CommonDAO {
	public static ResultSet toGetobjects(Statement statement, String strQuery){
		ResultSet rs = null;
		try{				
			 rs = statement.executeQuery(strQuery);
		}catch( Exception ex ){
			ex.printStackTrace();
		}		
		return rs;
		
	}
	public static int toUpdateobjects(Statement statement, String strQuery) {
		int nUpdate = 0;
		try{				
			nUpdate = statement.executeUpdate(strQuery);
		}catch( Exception ex ){
			ex.printStackTrace();
		}		
		return nUpdate;
	}
}
