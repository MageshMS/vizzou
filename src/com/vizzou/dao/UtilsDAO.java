package com.vizzou.dao;

import java.util.List;

import org.springframework.orm.hibernate3.HibernateTemplate;

import com.vizzou.bean.FriendsListEpisodeBean;

public class UtilsDAO {
	HibernateTemplate template;
	
	public HibernateTemplate getTemplate() {
		return template;
	}
	public void setTemplate(HibernateTemplate template) {
		this.template = template;
	}
	public void saveObject(Object obj){
		template.save(obj);
	}
	
	public void updateJob(Object obj){
		template.update(obj);
	}
	public void deleteJob(Object obj){
		template.delete(obj);
	}
	public void saveUpdateObject(Object obj){
		template.saveOrUpdate(obj);
	}
	public void saveCollectionAllObject(List<FriendsListEpisodeBean> alFriends){
		template.saveOrUpdateAll(alFriends);
	}
}
