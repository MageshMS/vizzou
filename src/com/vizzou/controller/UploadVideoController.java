package com.vizzou.controller;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import com.vizzou.bean.EpisodeInfoBean;
import com.vizzou.bean.FriendsListEpisodeBean;
import com.vizzou.model.EpisodeDetailsModel;
import com.vizzou.model.UploadFileModel;
import com.vizzou.model.UserModel;
import com.vizzou.utils.Constants;
import com.vizzou.utils.DataBaseManager;
import com.vizzou.utils.UtilsFactory;

@Path("/EPISODES")
public class UploadVideoController {
	
	@POST
    @Path("/CREATEEPISODE")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response createEpisode( @FormDataParam("file") InputStream uploadedInputStream,
			@FormDataParam("file") FormDataContentDisposition videofileDetail,
			@Context ServletContext context, @FormDataParam("userid") String strUserID, 
			@FormDataParam("episodename") String strEpisodeName,
			@FormDataParam("tag") String strTag,
			@FormDataParam("keywords") String strKeyWord, 
			@FormDataParam("releasedate") Date dtReleaseDate, 
			@FormDataParam("imagefile") InputStream uploadedInputStreamImage,
			@FormDataParam("imagefile") FormDataContentDisposition imagefileDetail){
		JSONObject joReturn = null;
		Connection con = null;
		Statement statement = null;
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context1.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			
			if( !UserModel.isEpisodeNameIsExist(statement,strEpisodeName) ){
				String strFileName = UploadFileModel.uploadVideo(uploadedInputStream, videofileDetail.getFileName(), Constants.strUploadFileDirectory, true);
				
				String strImageGetFileName = "noimage.jpg";
				if(uploadedInputStreamImage != null){
					strImageGetFileName = imagefileDetail.getFileName();
					UploadFileModel.uploadVideo(uploadedInputStreamImage, imagefileDetail.getFileName(), Constants.strImageUploadFileDirectory, false);
				}
				
				EpisodeInfoBean vBean = new EpisodeInfoBean();
				
				vBean.setStrvideoname(strFileName);
				vBean.setStrVideoRefName(videofileDetail.getFileName());
				vBean.setStrImageName(strImageGetFileName);
				vBean.setStrUserId(strUserID);
				
				vBean.setDtReleaseDate(dtReleaseDate);
				vBean.setStrEpisodeName(strEpisodeName);
				vBean.setStrKeyWords(strKeyWord);
				vBean.setStrTagName(strTag);
				
				vBean.setStrCreated_by(strUserID);
				vBean.setdCreated_On(new Date());
				joReturn = UserModel.saveVideoInfo(vBean);
			}else{
				joReturn = UtilsFactory.getJSONFailureReturn("Episode name is already exists");
			}
		}catch( Exception e){
			joReturn = UtilsFactory.getJSONFailureReturn(e.getMessage());
			
			System.out.println("Exception is in createEpisode : " + e.getMessage());
			e.printStackTrace();
		}finally{
			DataBaseManager.closeStatement(statement);
			DataBaseManager.closeConnection(con);
		}
		return	Response.status(200).entity( joReturn.toString() ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	
	@POST
    @Path("/CREATEFRIENDSEPISODE")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response createFriendsEpisode( @FormDataParam("file") InputStream uploadedInputStream, 
			@FormDataParam("file") FormDataContentDisposition videofileDetail,
			@Context ServletContext context, 
			@FormDataParam("userid") String strUserID,
			@FormDataParam("episodename") String strEpisodeName,
			@FormDataParam("tag") String strTag,
			@FormDataParam("keywords") String strKeyWord,
			@FormDataParam("releasedate") Date dtReleaseDate,
			@FormDataParam("friendsId") String strFriendsIds,
			@FormDataParam("imagefile") InputStream uploadedInputStreamImage,
			@FormDataParam("imagefile") FormDataContentDisposition imagefileDetail){
		JSONObject joReturn = null;
		Connection con = null;
		Statement statement = null;
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context1.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			
			if( !UserModel.isEpisodeNameIsExist(statement,strEpisodeName) ){
				String strFileName = UploadFileModel.uploadVideo(uploadedInputStream, videofileDetail.getFileName(), Constants.strUploadFileDirectory, true);
				
				String strImageGetFileName = "noimage.jpg";
				if(uploadedInputStreamImage != null){
					strImageGetFileName = imagefileDetail.getFileName();
					String strImageFileName = UploadFileModel.uploadVideo(uploadedInputStreamImage, imagefileDetail.getFileName(), Constants.strImageUploadFileDirectory, false);
				}
				
				EpisodeInfoBean vBean = new EpisodeInfoBean();
				
				vBean.setStrvideoname(strFileName);
				vBean.setStrVideoRefName(videofileDetail.getFileName());
				vBean.setStrImageName(strImageGetFileName);
				vBean.setStrUserId(strUserID);
				
				vBean.setDtReleaseDate(dtReleaseDate);
				vBean.setStrEpisodeName(strEpisodeName);
				vBean.setStrKeyWords(strKeyWord);
				vBean.setStrTagName(strTag);
				
				vBean.setStrCreated_by(strUserID);
				vBean.setdCreated_On(new Date());
				joReturn = UserModel.saveVideoInfo(vBean);
				
				
				
				//JSONObject joFriendsList = (JSONObject)new JSONParser().parse(strFriendsIds);
				System.out.println(strFriendsIds);
				String[] strFriendsList = strFriendsIds.split(",");
				List<FriendsListEpisodeBean> alFriends = new ArrayList<FriendsListEpisodeBean>();
				FriendsListEpisodeBean fbean = null;
				for( int a = 0; a < strFriendsList.length; a++ ){
					fbean  = new FriendsListEpisodeBean();
					fbean.setStrUserId(strUserID);
					fbean.setStrEpisodeId(vBean.getnId()+"");
					fbean.setStrFriendId(strFriendsList[a]);
					fbean.setDtCreatedOn(new Date());
					fbean.setStrCreatedBy(strUserID);
					alFriends.add(fbean);
					
				}
				joReturn = UserModel.saveFriendsShareInfo(alFriends);
			}else{
				joReturn = UtilsFactory.getJSONFailureReturn("Episode name is already exists");
			}
		}catch( Exception e){
			joReturn = UtilsFactory.getJSONFailureReturn(e.getMessage());
			
			System.out.println("Exception is in createFriendsEpisode : " + e.getMessage());
			e.printStackTrace();
		}finally{
			DataBaseManager.closeStatement(statement);
			DataBaseManager.closeConnection(con);
		}
		return	Response.status(200).entity( joReturn.toString() ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	
	@POST
    @Path("/UPDATEEPISODE")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response updateEpisodeVideo( @FormDataParam("file") InputStream uploadedInputStream, 
			@FormDataParam("file") FormDataContentDisposition videofileDetail,
			@Context ServletContext context,
			@FormDataParam("episodeId") int nEpId,
			@FormDataParam("userid") String strUserID,
			@FormDataParam("tag") String strTag,
			@FormDataParam("keywords") String strKeyWord,
			@FormDataParam("imagefile") InputStream uploadedInputStreamImage,
			@FormDataParam("imagefile") FormDataContentDisposition imagefileDetail){
		JSONObject joReturn = null;
		Connection con = null;
		Statement statement = null;
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context1.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			String strFileName = UploadFileModel.uploadVideo(uploadedInputStream, videofileDetail.getFileName(), Constants.strUploadFileDirectory, true);
			
			String strImageGetFileName = "noimage.jpg";
			if(uploadedInputStreamImage != null){
				strImageGetFileName = imagefileDetail.getFileName();
				String strImageFileName = UploadFileModel.uploadVideo(uploadedInputStreamImage, imagefileDetail.getFileName(), Constants.strImageUploadFileDirectory, false);
			}
			
			EpisodeInfoBean vBean = new EpisodeInfoBean();
			vBean.setnId(nEpId);
			vBean.setStrvideoname(strFileName);
			vBean.setStrVideoRefName(videofileDetail.getFileName());
			vBean.setStrImageName(strImageGetFileName);
			vBean.setStrKeyWords(strKeyWord);
			vBean.setStrModified_by(strUserID);
			vBean.setdModified_On(new Date());
			joReturn = UserModel.updateEpisode(vBean, statement);
			
		}catch( Exception e){
			joReturn = UtilsFactory.getJSONFailureReturn(e.getMessage());
			
			System.out.println("Exception is in updateEpisodeVideo : " + e.getMessage());
			e.printStackTrace();
		}finally{
			DataBaseManager.closeStatement(statement);
			DataBaseManager.closeConnection(con);
		}
		return	Response.status(200).entity( joReturn.toString() ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	
	@POST
    @Path("{EPISODEID}/{USERID}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getEpisodeDetails(@PathParam("EPISODEID") int nEpisodeId, @PathParam("USERID") String strUserId){
		JSONObject joReturn = null;
		Connection con = null;
		Statement statement = null;
		String strReurn = "";
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context1.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			JSONArray jaEpisodeDetails = EpisodeDetailsModel.getEpisodeDetails(statement,nEpisodeId,strUserId);
			strReurn = "{\"result\":"+jaEpisodeDetails.toString()+"}";
			
		}catch( Exception e){
			//joReturn = UtilsFactory.getJSONFailureReturn(e.getMessage());
			strReurn = "{\"result\":"+e.getMessage()+"}";
			System.out.println("Exception is in getEpisodeDetails : " + e.getMessage());
			e.printStackTrace();
		}finally{
			DataBaseManager.closeStatement(statement);
			DataBaseManager.closeConnection(con);
		}
		return	Response.status(200).entity( strReurn ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	
	@POST
    @Path("/GETEPISODES")
	//@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response getAllEpisodeDetails(@FormDataParam("Limit") int nLimit,
			@FormDataParam("Offset") int nOffset,
			@FormDataParam("Tag") String strTag, @FormDataParam("UserId") String strUserIds ){
		JSONObject joReturn = null;
		Connection con = null;
		Statement statement = null;
		String strReurn = "";
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context1.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			
			//JSONObject joEpisodes = (JSONObject)new JSONParser().parse(strEpisodeInfo);
			//int nLimit = Integer.parseInt((joEpisodes.get("Limit").toString()));
			//int nOffset = Integer.parseInt((joEpisodes.get("Offset").toString()));
			//String strTag = joEpisodes.get("Tag").toString();
			//String strUserId = joEpisodes.get("UserId").toString();
			int nTotalEpisodeCount = EpisodeDetailsModel.getTotalEpisodeDetailsBasedOnTag(statement,strTag,strUserIds);
			JSONArray jaEpisodeDetails = EpisodeDetailsModel.getEpisodeDetailsBasedOnTag(statement,strTag,strUserIds,nLimit,nOffset);
			strReurn = "{\"count\" :"+nTotalEpisodeCount+", \"result\":"+jaEpisodeDetails.toString()+"}";
			
		}catch( Exception e){
			//joReturn = UtilsFactory.getJSONFailureReturn(e.getMessage());
			strReurn = "{\"result\":"+e.getMessage()+"}";
			System.out.println("Exception is in getEpisodeDetails : " + e.getMessage());
			e.printStackTrace();
		}finally{
			DataBaseManager.closeStatement(statement);
			DataBaseManager.closeConnection(con);
		}
		return	Response.status(200).entity( strReurn ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	
	@POST
    @Path("/GETFRIENDSEPISODES")
	//@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response getAllFriendsEpisodeDetails(@FormDataParam("Limit") int nLimit,
			@FormDataParam("Offset") int nOffset,
			@FormDataParam("Tag") String strTag, @FormDataParam("UserId") String strUserIds){
		JSONObject joReturn = null;
		Connection con = null;
		Statement statement = null;
		String strReurn = "";
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context1.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			
			//JSONObject joEpisodes = (JSONObject)new JSONParser().parse(strEpisodeInfo);
			//int nLimit = Integer.parseInt((joEpisodes.get("Limit").toString()));
			//int nOffset = Integer.parseInt((joEpisodes.get("Offset").toString()));
			//String strTag = joEpisodes.get("Tag").toString();
			//String strUserId = joEpisodes.get("UserId").toString();
			int nTotalEpisodeCount = EpisodeDetailsModel.getTotalFriendsEpisodeDetails(statement,strTag,strUserIds);
			JSONArray jaEpisodeDetails = EpisodeDetailsModel.getFriendsEpisodeDetails(statement,strTag,strUserIds,nLimit,nOffset);
			strReurn = "{\"count\" :"+nTotalEpisodeCount+", \"result\":"+jaEpisodeDetails.toString()+"}";
			
		}catch( Exception e){
			//joReturn = UtilsFactory.getJSONFailureReturn(e.getMessage());
			strReurn = "{\"result\":"+e.getMessage()+"}";
			System.out.println("Exception is in getEpisodeDetails : " + e.getMessage());
			e.printStackTrace();
		}finally{
			DataBaseManager.closeStatement(statement);
			DataBaseManager.closeConnection(con);
		}
		return	Response.status(200).entity( strReurn ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	@POST
    @Path("/GETOPENEPISODES")
	//@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response getAllOpenEpisodeDetails( @FormDataParam("Limit") int nLimit,
			@FormDataParam("Offset") int nOffset,
			@FormDataParam("Tag") String strTag, @FormDataParam("UserId") String strUserIds ){
		JSONObject joReturn = null;
		Connection con = null;
		Statement statement = null;
		String strReurn = "";
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context1.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			
			//JSONObject joEpisodes = (JSONObject)new JSONParser().parse(strEpisodeInfo);
			//int nLimit = Integer.parseInt((joEpisodes.get("Limit").toString()));
			//int nOffset = Integer.parseInt((joEpisodes.get("Offset").toString()));
			//String strTag = joEpisodes.get("Tag").toString();
			//String strUserId = joEpisodes.get("UserId").toString();
			int nTotalEpisodeCount = EpisodeDetailsModel.getTotalOpenEpisodeDetails(statement,strTag,strUserIds);
			JSONArray jaEpisodeDetails = EpisodeDetailsModel.getOpenEpisodeDetails(statement,strTag,strUserIds,nLimit,nOffset);
			strReurn = "{\"count\" :"+nTotalEpisodeCount+", \"result\":"+jaEpisodeDetails.toString()+"}";
			
		}catch( Exception e){
			//joReturn = UtilsFactory.getJSONFailureReturn(e.getMessage());
			strReurn = "{\"result\":"+e.getMessage()+"}";
			System.out.println("Exception is in getEpisodeDetails : " + e.getMessage());
			e.printStackTrace();
		}finally{
			DataBaseManager.closeStatement(statement);
			DataBaseManager.closeConnection(con);
		}
		return	Response.status(200).entity( strReurn ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	
	@POST
    @Path("/SERACHOPENEPISODES")
	//@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response searchAllOpenEpisodeDetails( @FormDataParam("Limit") int nLimit,
			@FormDataParam("Offset") int nOffset,
			@FormDataParam("query") String strQuery){
		JSONObject joReturn = null;
		Connection con = null;
		Statement statement = null;
		String strReurn = "";
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context1.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			
			//JSONObject joEpisodes = (JSONObject)new JSONParser().parse(strEpisodeInfo);
			//int nLimit = Integer.parseInt((joEpisodes.get("Limit").toString()));
			//int nOffset = Integer.parseInt((joEpisodes.get("Offset").toString()));
			//String strTag = joEpisodes.get("Tag").toString();
			//String strUserId = joEpisodes.get("UserId").toString();
			int nTotalEpisodeCount = EpisodeDetailsModel.getTotalOpenEpisodes(statement,strQuery);
			JSONArray jaEpisodeDetails = EpisodeDetailsModel.getOpenEpisodes(statement,strQuery,nLimit,nOffset);
			strReurn = "{\"count\" :"+nTotalEpisodeCount+", \"result\":"+jaEpisodeDetails.toString()+"}";
			
		}catch( Exception e){
			//joReturn = UtilsFactory.getJSONFailureReturn(e.getMessage());
			strReurn = "{\"result\":"+e.getMessage()+"}";
			System.out.println("Exception is in getEpisodeDetails : " + e.getMessage());
			e.printStackTrace();
		}finally{
			DataBaseManager.closeStatement(statement);
			DataBaseManager.closeConnection(con);
		}
		return	Response.status(200).entity( strReurn ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	
}
