package com.vizzou.controller;


import java.io.InputStream;
import java.sql.Connection;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import com.vizzou.bean.FollowFriendBean;
import com.vizzou.bean.UserInfoBean;
import com.vizzou.bean.EpisodeInfoBean;
import com.vizzou.model.EpisodeDetailsModel;
import com.vizzou.model.UploadFileModel;
import com.vizzou.model.UserModel;
import com.vizzou.utils.Constants;
import com.vizzou.utils.DataBaseManager;
import com.vizzou.utils.UtilsFactory;

@Path("/USER")
public class UserController {
	@POST
    @Path("/USERREGISTRATION")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response getInstitution( @FormDataParam("firstname") String strFirstName, @FormDataParam("lastname") String strLastName,
			@FormDataParam("age") int nAge, @FormDataParam("gender") String strGender, @FormDataParam("latitude") double dLatitude, 
			@FormDataParam("longitude") double dLongitude, @FormDataParam("email") String strEmail, @FormDataParam("password") String strPassword){
		JSONObject joReturn = null;
		Connection con = null;
		Statement statement = null;
		try{
			//System.out.println("strFirstName : " + strFirstName);
			UserInfoBean userBean = new UserInfoBean();
			userBean.setStrFirstName(strFirstName);
			userBean.setStrLastName(strLastName);
			userBean.setStrGender(strGender);
			userBean.setnAge(nAge);
			userBean.setStrEmail(strEmail);
			userBean.setdLatitude(dLatitude);
			userBean.setdLongitude(dLongitude);
			userBean.setStrPassword(strPassword);
			userBean.setDtCreatedOn(new Date());
			//userBean.setStrCreatedBy(strCreatedBy);
			
			
			ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			
			if( !UserModel.isUserAlreadyExist(statement, userBean) ){
				userBean = UserModel.createNewUser(userBean);
				if( userBean.getnUid() > 0 ){
					HashMap<String, String> hUserData = new HashMap<String, String>();
					hUserData.put("FirstName", userBean.getStrFirstName());
					hUserData.put("LastName", userBean.getStrLastName());
					hUserData.put("Email", userBean.getStrEmail());
					hUserData.put("UserId", userBean.getStrUserId());
					hUserData.put("UId", userBean.getnUid()+"");
					joReturn = UtilsFactory.getJSONSuccessReturn( "", hUserData);
				}
			}else{
				joReturn = UtilsFactory.getJSONFailureReturn("User already exists");
			}
			
		}catch( Exception ex){
			ex.printStackTrace();
			joReturn = UtilsFactory.getJSONFailureReturn(ex.getMessage());
		}finally{
			DataBaseManager.closeStatement(statement);
			DataBaseManager.closeConnection(con);
		}
	
		return	Response.status(200).entity( joReturn.toString() ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	
	@POST
    @Path("/UPDATEUSER")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response updateUserProfile( @FormDataParam("uid") int nUid, @FormDataParam("userid") String strUserId,@FormDataParam("firstname") String strFirstName, @FormDataParam("lastname") String strLastName,
			@FormDataParam("email") String strEmail,@FormDataParam("phoneno") String strPhoneNo,@FormDataParam("age") int nAge,@FormDataParam("gender") String strGender, @FormDataParam("password") String strPassword, @FormDataParam("profilepic") InputStream uploadedInputStream,
			@FormDataParam("profilepic") FormDataContentDisposition profilePicDetail,
			@Context ServletContext context){
		String strReturn = "";
		try{
			//System.out.println("strFirstName : " + strFirstName);
			
			UserInfoBean userBean = new UserInfoBean();
			userBean.setnUid(nUid);
			userBean.setStrUserId(strUserId);
			userBean.setStrFirstName(strFirstName);
			userBean.setStrLastName(strLastName);
			userBean.setStrEmail(strEmail);
			userBean.setStrGender(strGender);
			userBean.setStrPassword(strPassword);
			userBean.setnAge(nAge);
			userBean.setStrPhoneNo(strPhoneNo);
			userBean.setStrModifiedBy(strUserId);
			userBean.setDtModifiedOn(new Date());
			String strImageFileName = UploadFileModel.uploadVideo(uploadedInputStream, profilePicDetail.getFileName(), Constants.strImageUploadFileDirectory, false);
			userBean.setStrProfilePic(strImageFileName);
			
			userBean = UserModel.updateUserProfile(userBean);
			strReturn = "{\"result\": [ { \"success\":\"Successfully inserted\"}] }";
		}catch( Exception ex){
			strReturn = "{\"result\": [ { \"error\":\""+ex.getMessage()+"\"}] }";
			ex.printStackTrace();
		}finally{
		}
	
		return	Response.status(200).entity( strReturn).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	
	@POST
    @Path("/USERLOGIN")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response loginValidation( @FormDataParam("email") String strEmail, @FormDataParam("password") String strPassword){
		JSONObject joReturn = null;
		String strReturn = "";
		Connection con = null;
		Statement statement = null;
		//String strReurn = "";
		try{
			//System.out.println("strUserInfo : " + strUserInfo);
			//JSONObject joUser = (JSONObject)new JSONParser().parse(strUserInfo);
			UserInfoBean userBean = new UserInfoBean();
			userBean.setStrEmail(strEmail);
			userBean.setStrPassword(strPassword);
			
			ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			
			JSONArray joLoginDetails = UserModel.loginValidation( statement, userBean );
			
			JSONObject joUserData = new JSONObject();
			joUserData.put("LoginDetails", joLoginDetails);
			if( joLoginDetails.size() > 0 ){
				strReturn = "{\"result\":"+joLoginDetails.toString()+"}";
				joReturn = UtilsFactory.getJSONSuccessReturn("data", joUserData);
			}else{
				strReturn = "{\"result\": [ { \"error\":\"Invalid User Name or Password\"}] }";
				joReturn = UtilsFactory.getJSONFailureReturn("Invalid User Name or Password");
			}
			
		}catch( Exception ex){
			strReturn = "{\"result\":"+ex.getMessage()+"}";
			ex.printStackTrace();
			joReturn = UtilsFactory.getJSONFailureReturn(ex.getMessage());
		}finally{
			DataBaseManager.closeStatement(statement);
			DataBaseManager.closeConnection(con);
		}
	
		return	Response.status(200).entity( strReturn ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	@POST
    @Path("/GETUSERLIST")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response getAllUserDetails(@FormDataParam("LIMIT") int nLimit, @FormDataParam("OFFSET") int nOffset ){
		Connection con = null;
		Statement statement = null;
		String strReurn = "";
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context1.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			
			int nTotalUserCount = UserModel.getTotalUserCount(statement);
			JSONArray jaUserDetails = UserModel.getUserDetails(statement,nLimit,nOffset);
			strReurn = "{\"count\" :"+nTotalUserCount+", \"result\":"+jaUserDetails.toString()+"}";
			
		}catch( Exception e){
			//joReturn = UtilsFactory.getJSONFailureReturn(e.getMessage());
			strReurn = "{\"result\":"+e.getMessage()+"}";
			System.out.println("Exception is in getAllUserDetails : " + e.getMessage());
			e.printStackTrace();
		}finally{
			DataBaseManager.closeStatement(statement);
			DataBaseManager.closeConnection(con);
		}
		return	Response.status(200).entity( strReurn ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	
	@POST
    @Path("/VIEWUSERPROFILE")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response getUserProfile(@FormDataParam("userId") String strUserId ){
		Connection con = null;
		Statement statement = null;
		String strReurn = "";
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context1.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			
			JSONArray jaUserDetails = UserModel.getUserProfile(statement,strUserId);
			strReurn = "{\"result\":"+jaUserDetails.toString()+"}";
			
		}catch( Exception e){
			//joReturn = UtilsFactory.getJSONFailureReturn(e.getMessage());
			strReurn = "{\"result\":"+e.getMessage()+"}";
			System.out.println("Exception is in getAllUserDetails : " + e.getMessage());
			e.printStackTrace();
		}finally{
			DataBaseManager.closeStatement(statement);
			DataBaseManager.closeConnection(con);
		}
		return	Response.status(200).entity( strReurn ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	
	@POST
    @Path("/SEARCHFRIENDS")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response searchFriends(@FormDataParam("query") String strQuery, @FormDataParam("userID") String strUserId ){
		Connection con = null;
		Statement statement = null;
		String strReurn = "";
		try{
			ApplicationContext context1 = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context1.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			
			JSONArray jaUserDetails = UserModel.getSearchFriends(statement,strQuery,strUserId);
			strReurn = "{\"result\":"+jaUserDetails.toString()+"}";
			
		}catch( Exception e){
			//joReturn = UtilsFactory.getJSONFailureReturn(e.getMessage());
			strReurn = "{\"result\":"+e.getMessage()+"}";
			System.out.println("Exception is in searchFriends : " + e.getMessage());
			e.printStackTrace();
		}finally{
			DataBaseManager.closeStatement(statement);
			DataBaseManager.closeConnection(con);
		}
		return	Response.status(200).entity( strReurn ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	
	@POST
    @Path("/FOLLOWFRIEND")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response followFriend(@FormDataParam("userId") String strUserId, @FormDataParam("friendId") String strFriendId ){
	
		String strReurn = "";
		try{
			
			FollowFriendBean followBean = new FollowFriendBean();
			followBean.setStrUserId(strUserId);
			followBean.setStrFriendId(strFriendId);
			followBean.setStrCreatedBy(strUserId);
			followBean.setDtCreadtedOn(new Date());
					
			boolean bReturn = UserModel.setFollowingFriend(followBean);
			if(bReturn){
				strReurn = "{\"result\":\"Now following your friend\"}";
			}else{
				strReurn = "{\"result\":\"Already following\"}";
			}
			
			
		}catch( Exception e){
			//joReturn = UtilsFactory.getJSONFailureReturn(e.getMessage());
			strReurn = "{\"result\":"+e.getMessage()+"}";
			System.out.println("Exception is in followFriend : " + e.getMessage());
			e.printStackTrace();
		}
		return	Response.status(200).entity( strReurn ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	
	@POST
    @Path("/UNFOLLOWFRIEND")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response unFollowFriend(@FormDataParam("uId") int nUId ){
	
		String strReurn = "";
		try{
			
			FollowFriendBean followBean = new FollowFriendBean();
			followBean.setnId(nUId);
			boolean bReturn = UserModel.removeFollowingFriend(followBean);
			
			if(bReturn){
				strReurn = "{\"result\":\"removed friend from follow list\"}";
			}
		}catch( Exception e){
			//joReturn = UtilsFactory.getJSONFailureReturn(e.getMessage());
			strReurn = "{\"result\":"+e.getMessage()+"}";
			System.out.println("Exception is in followFriend : " + e.getMessage());
			e.printStackTrace();
		}
		
		return	Response.status(200).entity( strReurn ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}
	
	/*@POST
    @Path("/GETALLEPISODES")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllEpisodes( ){
		JSONObject joReturn = null;
		Connection con = null;
		Statement statement = null;
		try{			
			ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext.xml");
			DataBaseManager jdbcObj=(DataBaseManager)context.getBean("jdbcTemplate");
			con = jdbcObj.giveConnection();
			statement = con.createStatement();
			int nTotalRecord = UserModel.getTotalAllEpisodes( statement );
			JSONArray joLoginDetails = UserModel.getAllEpisodes( statement );
			
			JSONObject joUserData = new JSONObject();
			joUserData.put("Count", joLoginDetails);
			joUserData.put("Records", joLoginDetails);
			if( joLoginDetails.size() > 0 ){
				joReturn = UtilsFactory.getJSONSuccessReturn("data", joUserData);
			}else{
				joReturn = UtilsFactory.getJSONFailureReturn("No Records Found");
			}
			
		}catch( Exception ex){
			ex.printStackTrace();
			joReturn = UtilsFactory.getJSONFailureReturn(ex.getMessage());
		}finally{
			DataBaseManager.closeStatement(statement);
			DataBaseManager.closeConnection(con);
		}
	
		return	Response.status(200).entity( joReturn.toString() ).
				header("Access-Control-Allow-Origin","*").
				header("Access-Control-Allow-Credentials","true").
				header("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS").
				header("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers").
				build();
	}*/

}
